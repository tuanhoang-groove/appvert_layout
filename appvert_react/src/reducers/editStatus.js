const statusInitialState = true;
const status = (state = statusInitialState, action) => {
    switch (action.type) {
        case "CHANGE_EDIT":
            return !state
        default:
            return state
    }
}

export default status;