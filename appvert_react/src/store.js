var redux = require('redux');

const statusInitialState = true;
const status = (state = statusInitialState, action) => {
    switch (action.type) {
        case "CHANGE_EDIT":
            return !state;
        default:
            return state;
    }
}

const allReducers = redux.combineReducers({
    status: status
})

var store = redux.createStore(allReducers);
store.subscribe(() => {
    //console.log(JSON.stringify(store.getState()));
})

export default store;