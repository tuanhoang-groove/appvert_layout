import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";

import { Route, Switch } from "react-router-dom";
import Register from './component/page/Register';
import Landing from './component/page/Landing';
import Login from './component/page/Login';
import Billing from './component/page/Billing';
import LandingLayout from './component/LandingLayout';
import LandingUpgradeLayout from './component/LandingUpgradeLayout';
import DashboardLayout from './component/DashboardLayout';
import LoginLayout from './component/LoginLayout';

import Index from './component/page/index';
import Dashboard from './component/page/Dashboard';
import DashboardUserList from './component/page/DashboardUserList';
import DashboardUserCreate from './component/page/DashboardUserCreate';
import DashboardProfile from './component/page/DashboardProfile';
import DashboardProfileUser from './component/page/DashboardProfileUser';
import DashboardProfileUserEdit from './component/page/DashboardProfileUserEdit';
import DashboardProfileEdit from './component/page/DashboardProfileEdit';
import DashboardMobileAppCreate from './component/page/DashboardMobileAppCreate';
import DashboardMobileAppList from './component/page/DashboardMobileAppList';
import DashboardAddPayment from './component/page/DashboardAddPayment';
import DashboardAdCampaignsList from './component/page/DashboardAdCampaignsList';
import DashboardAdCampaignsCreate from './component/page/DashboardAdCampaignsCreate';
import DashboardAdCampaignsCreate2B from './component/page/DashboardAdCampaignsCreate2B';
import DashboardSubscriptionPlanList from './component/page/DashboardSubscriptionPlanList';
import DashboardSubscriptionPlanCreate from './component/page/DashboardSubscriptionPlanCreate';
import DashboardChangePassword from './component/page/ChangePassword';
import DashboardSubscriptionPlanDetail from './component/page/DashboardSubscriptionPlanDetail';
import DashboardMobileAppDetail from './component/page/DashboardMobileAppDetail';
import DashboardAdCampaignsDetail from './component/page/DashboardAdCampaignsDetail';


const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
    <Route {...rest} render={props => (
        <Layout>
            <Component {...props} />
        </Layout>
    )} />
)

const App = () => (
    <Router>
        <Switch>
            <AppRoute exact path='/index' layout={DashboardLayout} component={Index}/>
            <AppRoute exact path='/Dashboard' layout={DashboardLayout} component={Dashboard}/>
            <AppRoute exact path='/DashboardUserList' layout={DashboardLayout} component={DashboardUserList}/>
            <AppRoute exact path='/DashboardUserCreate' layout={DashboardLayout} component={DashboardUserCreate}/>
            <AppRoute exact path='/DashboardProfile' layout={DashboardLayout} component={DashboardProfile}/>
            <AppRoute exact path='/DashboardProfileUser' layout={DashboardLayout} component={DashboardProfileUser}/>
            <AppRoute exact path='/DashboardProfileUserEdit' layout={DashboardLayout} component={DashboardProfileUserEdit}/>
            <AppRoute exact path='/DashboardProfileEdit' layout={DashboardLayout} component={DashboardProfileEdit}/>
            <AppRoute exact path='/DashboardMobileAppCreate' layout={DashboardLayout} component={DashboardMobileAppCreate}/>
            <AppRoute exact path='/DashboardMobileAppList' layout={DashboardLayout} component={DashboardMobileAppList}/>
            <AppRoute exact path='/DashboardAddPayment' layout={DashboardLayout} component={DashboardAddPayment}/>
            <AppRoute exact path='/DashboardAdCampaignsList' layout={DashboardLayout} component={DashboardAdCampaignsList}/>
            <AppRoute exact path='/DashboardAdCampaignsCreate' layout={DashboardLayout} component={DashboardAdCampaignsCreate}/>
            <AppRoute exact path='/DashboardAdCampaignsCreate2B' layout={DashboardLayout} component={DashboardAdCampaignsCreate2B}/>
            <AppRoute exact path='/DashboardSubscriptionPlanList' layout={DashboardLayout} component={DashboardSubscriptionPlanList}/>
            <AppRoute exact path='/DashboardSubscriptionPlanCreate' layout={DashboardLayout} component={DashboardSubscriptionPlanCreate}/>
            <AppRoute exact path='/DashboardChangePassword' layout={DashboardLayout} component={DashboardChangePassword}/>
            <AppRoute exact path='/DashboardSubscriptionPlanDetail' layout={DashboardLayout} component={DashboardSubscriptionPlanDetail}/>
            <AppRoute exact path='/DashboardMobileAppDetail' layout={DashboardLayout} component={DashboardMobileAppDetail}/>
            <AppRoute exact path='/DashboardAdCampaignsDetail' layout={DashboardLayout} component={DashboardAdCampaignsDetail}/>
            
            
            <AppRoute exact path="/" layout={LoginLayout} component={Login} />
            <AppRoute exact path="/Login" layout={LoginLayout} component={Login} />
            <AppRoute exact path="/Register" layout={LandingLayout} component={Register} />
            <AppRoute exact path="/Landing" layout={LandingUpgradeLayout} component={Landing} />
            <AppRoute exact path="/Billing" layout={LandingLayout} component={Billing} />
        </Switch>
    </Router>
)

// class App extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//         };
//     }

//     handleClick = () => {
//         this.setState({
//             statusDisplay: !this.state.statusDisplay
//         });
//     }

    // render() {
    //   return (
    //     <Router>
    //         <div className="App">
    //             {/* <Route path="/Register" component={Register} />
    //             <Route path="/Dashboard" component={PageContent} /> */}
    //             <div className="container-fluid body">
    //                 <div className="main_container">
    //                     <LeftPanel/>
    //                     <TopNav/>
    //                     <PageContent/>
    //                 </div>
    //             </div>
    //         </div>
    //     </Router>
    //   );
    // }
// }




export default App; 