import React, { Component } from 'react';
import { Route } from "react-router-dom";
import ModalRemoveTriggerConfirm from './page/ModalRemoveTriggerConfirm';
import ModalRemoveCardConfirm from './page/ModalRemoveCardConfirm';
import ModalAddTrigger from './page/ModalAddTrigger';
import Index from './page/index';
import Dashboard from './page/Dashboard';
import DashboardUserList from './page/DashboardUserList';
import DashboardUserCreate from './page/DashboardUserCreate';
import DashboardProfile from './page/DashboardProfile';
import DashboardProfileUser from './page/DashboardProfileUser';
import DashboardProfileUserEdit from './page/DashboardProfileUserEdit';
import DashboardProfileEdit from './page/DashboardProfileEdit';
import DashboardMobileAppCreate from './page/DashboardMobileAppCreate';
import DashboardMobileAppList from './page/DashboardMobileAppList';
import DashboardAddPayment from './page/DashboardAddPayment';
import DashboardAdCampaignsList from './page/DashboardAdCampaignsList';
import DashboardAdCampaignsCreate from './page/DashboardAdCampaignsCreate';
import DashboardAdCampaignsCreate2B from './page/DashboardAdCampaignsCreate2B';
import DashboardSubscriptionPlanList from './page/DashboardSubscriptionPlanList';
import DashboardSubscriptionPlanCreate from './page/DashboardSubscriptionPlanCreate';


class PageContent extends Component {
    render() {
        return (
            <div className="right_col" role="main">
                <Route exact path='/index' component={Index}/>
                <Route exact path='/' component={Dashboard}/>
                <Route exact path='/DashboardUserList' component={DashboardUserList}/>
                <Route exact path='/DashboardUserCreate' component={DashboardUserCreate}/>
                <Route exact path='/DashboardProfile' component={DashboardProfile}/>
                <Route exact path='/DashboardProfileUser' component={DashboardProfileUser}/>
                <Route exact path='/DashboardProfileUserEdit' component={DashboardProfileUserEdit}/>
                <Route exact path='/DashboardProfileEdit' component={DashboardProfileEdit}/>
                <Route exact path='/DashboardMobileAppCreate' component={DashboardMobileAppCreate}/>
                <Route exact path='/DashboardMobileAppList' component={DashboardMobileAppList}/>
                <Route exact path='/DashboardAddPayment' component={DashboardAddPayment}/>
                <Route exact path='/DashboardAdCampaignsList' component={DashboardAdCampaignsList}/>
                <Route exact path='/DashboardAdCampaignsCreate' component={DashboardAdCampaignsCreate}/>
                <Route exact path='/DashboardAdCampaignsCreate2B' component={DashboardAdCampaignsCreate2B}/>
                <Route exact path='/DashboardSubscriptionPlanList' component={DashboardSubscriptionPlanList}/>
                <Route exact path='/DashboardSubscriptionPlanCreate' component={DashboardSubscriptionPlanCreate}/>

                <ModalRemoveTriggerConfirm/>
                <ModalRemoveCardConfirm/>
                <ModalAddTrigger/>
            </div>
        );
    }
}

export default PageContent;