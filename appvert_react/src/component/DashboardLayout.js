import React, { Component } from 'react';
import BodyClassName from 'react-body-classname';
import LeftPanel from './LeftPanel';
import TopNav from './TopNav';
import ModalRemoveTriggerConfirm from './page/ModalRemoveTriggerConfirm';
import ModalRemoveCardConfirm from './page/ModalRemoveCardConfirm';
import ModalAddTrigger from './page/ModalAddTrigger';
import ModalChangePaymentMethod from './page/ModalChangePaymentMethod';
import ModalResetPassword from './page/ModalResetPassword';


class DashboardLayout extends Component {    
    render() {
        return (
            <BodyClassName className="dashboard nav-md">
            <div className="container-fluid body">
                <div className="main_container">
                    <LeftPanel/>
                    <TopNav/>
                    <div className="right_col" role="main">
                        {this.props.children}
                    </div>
                </div>
                <ModalRemoveTriggerConfirm/>
                <ModalRemoveCardConfirm/>
                <ModalAddTrigger/>
                <ModalChangePaymentMethod/>
                <ModalResetPassword/>
            </div>
            </BodyClassName>
        );
    }
}

export default DashboardLayout;