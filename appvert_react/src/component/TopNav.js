import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

export default class TopNav extends Component {
    render() {
        return (
            <div className="top_nav">
                <div className="nav_menu">
                    <nav>
                        <div className="nav toggle">
                            <a id="menu_toggle"><i className="menu_toggle" /></a>
                        </div>
                        <ul className="nav navbar-nav navbar-right">
                            <li>
                                <a href="" className="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span className="name">Sammy Le <span className="email">sammy.le@gmail.com</span></span>
                                    <i className="arrow_triangle-down" />
                                </a>
                                <ul className="dropdown-menu dropdown-usermenu pull-right">
                                    <li>
                                        <NavLink to="/DashboardProfile"><i className="menu-icon icon-profile" /> My Profile</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/DashboardChangePassword"><i className="menu-icon icon-changepass" /> Change Password</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/Landing"><i className="menu-icon icon-upgrade" /> Upgrade</NavLink>
                                    </li>
                                    <li>
                                        <NavLink to="/Login"><i className="menu-icon icon-logout" /> Logout</NavLink>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        )
    }
}
