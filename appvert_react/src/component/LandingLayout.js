import React, { Component } from 'react';
import './css/Landing.css';
import BodyClassName from 'react-body-classname';

class LandingLayout extends Component {

    
    render() {
        return (
            <BodyClassName className="">
            <div>
                {this.props.children}
            </div>
            </BodyClassName>
        );
    }
}

export default LandingLayout;