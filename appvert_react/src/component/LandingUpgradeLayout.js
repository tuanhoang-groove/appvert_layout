import React, { Component } from 'react';
import './css/Landing.css';
import BodyClassName from 'react-body-classname';

class LandingUpgradeLayout extends Component {
    render() {
        return (
            <BodyClassName className="landing">
            <div>
                {this.props.children}
            </div>
            </BodyClassName>
        );
    }
}

export default LandingUpgradeLayout;