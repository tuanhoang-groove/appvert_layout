import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class LeftPanel extends Component {
    render() {
        return (
            <div className="col-md-3 left_col">
                <div className="left_col scroll-view">
                    <div className="navbar nav_title" style={{ border: 0 }}>
                        <a href="" className="site_logo">Appvert</a>
                    </div>
                    <div className="clearfix" />
                    {/* sidebar menu */}
                    <div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
                        <div className="menu_section">
                            <ul className="nav side-menu">
                                <li className="active">
                                    <NavLink to="/Dashboard" ><i className="menu-icon icon-dashboard" /> Dashboard</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/DashboardSubscriptionPlanList" ><i className="menu-icon icon-plan" /> Subscription Plan</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/DashboardUserList" ><i className="menu-icon icon-user" /> User</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/DashboardMobileAppList" ><i className="menu-icon icon-mobile" /> Mobile Apps</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/DashboardAdCampaignsList" ><i className="menu-icon icon-campaign" /> Ad Campaigns</NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {/* /sidebar menu */}
                </div>
            </div>
        );
    }
}

export default LeftPanel;