import React, { Component } from 'react';
import BodyClassName from 'react-body-classname';

class LoginLayout extends Component {    
    render() {
        return (
            <BodyClassName className="login-page">
                <div className="container-fluid">
                    {this.props.children}
                </div>
            </BodyClassName>
        );
    }
}

export default LoginLayout;