import React, { Component } from 'react';

class DashboardAdCampaignsCreate2B extends Component {
    render() {
        return (
            <div>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="">Ad Campaigns List</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Create Ad Campaign</li>
                    </ol>
                </nav>
                <div className="clearfix" />
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <h3 className="h3-title">Create Ad Campaign</h3>
                        <div className="x_panel">
                            <div className="x_content">
                                <div role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" className="nav nav-tabs bar_tabs" role="tablist">
                                        <li className="active">
                                            <a href="#tab_general" id="general-tab" role="tab" data-toggle="tab" aria-expanded="true">General</a>
                                        </li>
                                        <li >
                                            <a href="#tab_triggers" role="tab" id="triggers-tab" data-toggle="tab" aria-expanded="false">Triggers</a>
                                        </li>
                                    </ul>
                                    <div id="myTabContent" className="tab-content pt-1 pb-1">
                                        <div role="tabpanel" className="tab-pane fade active in" id="tab_general" aria-labelledby="general-tab">
                                            <form action="" className="form-style form-style2">
                                                <div className="row">
                                                    <div className="col-md-8 col-sm-8 col-xs-12 mt-2">
                                                        <div className="row">
                                                            <div className="col-md-6 col-sm-6 col-xs-12">
                                                                <div className="form-group">
                                                                    <label htmlFor="name">Name<span>*</span></label>
                                                                    <input type="text" name="name" id="name" className="form-control" placeholder="" required />
                                                                </div>
                                                                <div className="form-group icon-right">
                                                                    <label htmlFor="linkurl">Link Url<span>*</span></label>
                                                                    <i className="icon-form icon_link" />
                                                                    <input type="text" name="linkurl" id="linkurl" className="form-control" placeholder="" required />
                                                                </div>
                                                            </div>
                                                            <div className="col-md-6 col-sm-6 col-xs-12">
                                                                <div className="form-group icon-right">
                                                                    <label htmlFor="imageurl">Image Url<span>*</span></label>
                                                                    <i className="icon-form icon_link" />
                                                                    <input type="text" name="imageurl" id="imageurl" className="form-control" placeholder="" required />
                                                                </div>
                                                                <div className="form-group">
                                                                    <label htmlFor="dimensions">Dimensions<span>*</span></label>
                                                                    <select className="form-control selectpicker" name="dimensions" id="dimensions">
                                                                        <option>Popup Ad</option>
                                                                        <option>Banner Ad</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row">
                                                            <div className="col-md-12 col-sm-12 col-xs-12">
                                                                <div className="form-group">
                                                                    <label htmlFor="description">Description</label>
                                                                    <textarea className="form-control" name="description" id="description" rows={3} defaultValue={""} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12 col-sm-12 col-xs-12 mt-1 btn-groups">
                                                            <a className="btn btn-secondary w150" href="" role="button">Back</a>
                                                            <button className="btn btn-primary" type="submit">Create</button>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4 col-sm-4 col-xs-12">
                                                        <div className="campaign-position">
                                                            <div className="mobile center">
                                                                <div className="popup-ad">
                                                                    <a href="javascript:void(0)"className="close"><i className="icon_close" /></a>
                                                                    <img src="/assets/css/img/dashboard/advertising.png" alt=""/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div role="tabpanel" className="tab-pane fade" id="tab_triggers" aria-labelledby="triggers-tab">
                                            <div className="row tab_triggers">
                                                <div className="col-md-12 col-sm-12 col-xs-12">
                                                    <a className="btn btn-primary btn-icon float-right" href="" role="button" data-toggle="modal" data-target="#addTriggerModal"><i className="icon-add-white" />Create</a>
                                                    <div className="table-section mb-3">
                                                        <h4 className="h4-title mt-2">Triggers</h4>
                                                        <table className="table mb-1">
                                                            <thead className="thead-light">
                                                                <tr>
                                                                    <th scope="col" className="none-sort">Name</th>
                                                                    <th scope="col">Valid from</th>
                                                                    <th scope="col">Valid to</th>
                                                                    <th scope="col">Ad Slot Identifier</th>
                                                                    <th scope="col">Frequency</th>
                                                                    <th scope="col">Delay</th>
                                                                    <th scope="col">Reset</th>
                                                                    <th scope="col">Created</th>
                                                                    <th scope="col">Modified</th>
                                                                    <th scope="col" className="none-sort">&nbsp;</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row" className="normal">Trigger Just One</th>
                                                                    <td>05/31/2018, 1:54 PM</td>
                                                                    <td>06/31/2018, 1:54 PM</td>
                                                                    <td>Start_justone</td>
                                                                    <td>Start_justone</td>
                                                                    <td>15 sec</td>
                                                                    <td>None</td>
                                                                    <td>06/31/2018, 1:54 PM</td>
                                                                    <td>06/31/2018, 1:54 PM</td>
                                                                    <td>
                                                                        <div className="btn-action">
                                                                            <a className="btn btn-primary" href="" role="button"><i className="icon-edit" /></a>
                                                                            <a className="btn btn-danger" href="" role="button" data-toggle="modal" data-target="#removeTriggerConfirm"><i className="icon_trash" /></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row" className="normal">Trigger Just One</th>
                                                                    <td>05/31/2018, 1:54 PM</td>
                                                                    <td>06/31/2018, 1:54 PM</td>
                                                                    <td>Start_justone</td>
                                                                    <td>Start_justone</td>
                                                                    <td>15 sec</td>
                                                                    <td>None</td>
                                                                    <td>06/31/2018, 1:54 PM</td>
                                                                    <td>06/31/2018, 1:54 PM</td>
                                                                    <td>
                                                                        <div className="btn-action">
                                                                            <a className="btn btn-primary" href="" role="button"><i className="icon-edit" /></a>
                                                                            <a className="btn btn-danger" href="" role="button" data-toggle="modal" data-target="#removeTriggerConfirm"><i className="icon_trash" /></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row" className="normal">Trigger Just One</th>
                                                                    <td>05/31/2018, 1:54 PM</td>
                                                                    <td>06/31/2018, 1:54 PM</td>
                                                                    <td>Start_justone</td>
                                                                    <td>Start_justone</td>
                                                                    <td>15 sec</td>
                                                                    <td>None</td>
                                                                    <td>06/31/2018, 1:54 PM</td>
                                                                    <td>06/31/2018, 1:54 PM</td>
                                                                    <td>
                                                                        <div className="btn-action">
                                                                            <a className="btn btn-primary" href="" role="button"><i className="icon-edit" /></a>
                                                                            <a className="btn btn-danger" href="" role="button" data-toggle="modal" data-target="#removeTriggerConfirm"><i className="icon_trash" /></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="table-section">
                                                        <h4 className="h4-title mt-2">List of Mobile Apps</h4>
                                                        <table className="table mb-1">
                                                            <thead className="thead-light">
                                                                <tr>
                                                                    <th scope="col" className="none-sort">Linked</th>
                                                                    <th scope="col">App Id</th>
                                                                    <th scope="col">Name</th>
                                                                    <th scope="col">App Key</th>
                                                                    <th scope="col">Device Type</th>
                                                                    <th scope="col" className="none-sort">App Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">
                                                                        <div className="form-check">
                                                                            <label className="form-check-label">
                                                                                <input className="form-check-input" name="" id="" type="checkbox" defaultValue="checkedValue" aria-label="Text for screen reader" />
                                                                                <span className="cr"><i className="cr-icon icon_check" /></span>
                                                                            </label>
                                                                        </div>
                                                                    </th>
                                                                    <td><a href="">1H90878</a></td>
                                                                    <td>Angry Bird</td>
                                                                    <td>87fa9d7ac3ac44f8a7d38c781d327fa5</td>
                                                                    <td>iOS</td>
                                                                    <td><a href=""className="btn btn-smaller btn-danger">Deactive</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">
                                                                        <div className="form-check">
                                                                            <label className="form-check-label">
                                                                                <input className="form-check-input" name="" id="" type="checkbox" defaultValue="checkedValue" aria-label="Text for screen reader" />
                                                                                <span className="cr"><i className="cr-icon icon_check" /></span>
                                                                            </label>
                                                                        </div>
                                                                    </th>
                                                                    <td><a href="">1H90878</a></td>
                                                                    <td>Angry Bird</td>
                                                                    <td>87fa9d7ac3ac44f8a7d38c781d327fa5</td>
                                                                    <td>iOS</td>
                                                                    <td><a href=""className="btn btn-smaller btn-success">Active</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">
                                                                        <div className="form-check">
                                                                            <label className="form-check-label">
                                                                                <input className="form-check-input" name="" id="" type="checkbox" defaultValue="checkedValue" aria-label="Text for screen reader" />
                                                                                <span className="cr"><i className="cr-icon icon_check" /></span>
                                                                            </label>
                                                                        </div>
                                                                    </th>
                                                                    <td><a href="">1H90878</a></td>
                                                                    <td>Angry Bird</td>
                                                                    <td>87fa9d7ac3ac44f8a7d38c781d327fa5</td>
                                                                    <td>iOS</td>
                                                                    <td><a href=""className="btn btn-smaller btn-danger">Deactive</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">
                                                                        <div className="form-check">
                                                                            <label className="form-check-label">
                                                                                <input className="form-check-input" name="" id="" type="checkbox" defaultValue="checkedValue" aria-label="Text for screen reader" />
                                                                                <span className="cr"><i className="cr-icon icon_check" /></span>
                                                                            </label>
                                                                        </div>
                                                                    </th>
                                                                    <td><a href="">1H90878</a></td>
                                                                    <td>Angry Bird</td>
                                                                    <td>87fa9d7ac3ac44f8a7d38c781d327fa5</td>
                                                                    <td>iOS</td>
                                                                    <td><a href=""className="btn btn-smaller btn-success">Active</a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div className="col-md-12 col-sm-12 col-xs-12 mt-1 pr-1 btn-groups">
                                                    <a className="btn btn-secondary w150" href="" role="button">Back</a>
                                                    <a className="btn btn-primary w150" href="" role="button">Save</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default DashboardAdCampaignsCreate2B;