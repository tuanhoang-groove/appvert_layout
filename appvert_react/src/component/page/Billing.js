import React, { Component } from 'react';
import './../css/Dashboard.css';
import HeaderLanding from './HeaderLanding';

class Register extends Component {
    render() {
        return (
            <div>
                <HeaderLanding />
                <div className="container">
                    <div className="row justify-content-between">
                        <div className="col-md-6 col-sm-6 col-xs-12 form-register">
                            <h2 className="title">Add Your Billing Infomation</h2>
                            <form action="" className="form-style form-style2">
                                <div role="tabpanel">
                                    <div className="row">
                                        <div className="col-md-12 col-sm-12 col-xs-12 select-payment-method">
                                            <p>Payment Method</p>
                                            <div className="form-check form-check-inline">
                                                <label className="form-check-label" data-target="#paymentPaypal">
                                                    <input className="form-check-input" type="radio" name="paymentmethod" defaultChecked id="paymentPaypalRadio" defaultValue="checkedValue" /><i className="icon-paypal-large" />
                                                </label>
                                            </div>
                                            <div className="form-check form-check-inline">
                                                <label className="form-check-label" data-target="#paymentCredit">
                                                    <input className="form-check-input" type="radio" name="paymentmethod" id="paymentCreditRadio" defaultValue="checkedValue" /> <span className="check-label">Credit/ Debit Card</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {/* Tab panes */}
                                    <div className="tab-content pd-0">
                                        <div role="tabpanel" className="tab-pane active" id="paymentPaypal">
                                            <div className="btn-groups justify-content-center mt-1" role="group" aria-label="Basic example">
                                                <button type="button" className="btn btn-primary w250">Sign in PayPal account</button>
                                            </div>
                                        </div>
                                        <div role="tabpanel" className="tab-pane" id="paymentCredit">
                                            <div className="row">
                                                <div className="col-md-12 col-sm-12 col-xs-12">
                                                    <div className="form-check form-check-style">
                                                        <label className="form-check-label" data-target="#cardPayment">
                                                            <input type="radio" className="form-check-input" name="selectpayment" id="cardPayment1" defaultValue="checkedValue" defaultChecked />
                                                            Card <span>**** **** *** 6789</span>
                                                        </label>
                                                    </div>
                                                    <div className="form-check form-check-style">
                                                        <label className="form-check-label" data-target="#cardPayment">
                                                            <input type="radio" className="form-check-input" name="selectpayment" id="cardPayment2" defaultValue="checkedValue" />
                                                            Card <span>**** **** *** 6789</span>
                                                        </label>
                                                    </div>
                                                    <div className="form-check form-check-style">
                                                        <label className="form-check-label" data-target="#addNewPayment">
                                                            <input className="form-check-input" type="radio" name="selectpayment" id="addNewPayment1" defaultValue="checkedValue" />
                                                            Add new card
                    </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tab-content pd-0">
                                                <div role="tabpanel" className="tab-pane active" id="cardPayment">
                                                    <div className="btn-groups justify-content-center mt-1" role="group" aria-label="Basic example">
                                                        <a href="/DashboardProfile" type="submit" className="btn btn-primary w150">Checkout</a>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" className="tab-pane" id="addNewPayment">
                                                    <div className="row">
                                                        <div className="col-md-6 col-sm-6 col-xs-12">
                                                            <div className="form-group">
                                                                <label htmlFor="fullname">Cardholder Name<span>*</span></label>
                                                                <input type="text" name="fullname" id="fullname" className="form-control" placeholder="Enter name"  required />
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="expiration">Expiration Date<span>*</span></label>
                                                                <input type="text" name="expiration" id="expiration" className="form-control" placeholder="mm/yy"  required />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6 col-sm-6 col-xs-12">
                                                            <div className="form-group">
                                                                <label htmlFor="cardnumber">Card Number<span>*</span></label>
                                                                <input type="number" name="cardnumber" id="cardnumber" className="form-control" placeholder="--/--/--/--"  required />
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="cvv">CVV Code<span>*</span></label>
                                                                <input type="text" name="cvv" id="cvv" className="form-control" placeholder="-"  required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="address1">Address 1<span>*</span></label>
                                                        <input type="text" name="address1" id="address1" className="form-control" placeholder="At least 8 characters"  required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="address2">Address 2<span>*</span></label>
                                                        <input type="text" name="address2" id="address2" className="form-control" placeholder="At least 8 characters"  required />
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-6 col-sm-6 col-xs-12">
                                                            <div className="form-group">
                                                                <label htmlFor="country">Country<span>*</span></label>
                                                                <select className="form-control selectpicker" name="country" id="country">
                                                                    <option>United States</option>
                                                                    <option>Ustralia</option>
                                                                    <option>Eorup</option>
                                                                </select>
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="state">State</label>
                                                                <input type="text" name="state" id="state" className="form-control" placeholder="-" />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6 col-sm-6 col-xs-12">
                                                            <div className="form-group">
                                                                <label htmlFor="country">City<span>*</span></label>
                                                                <select className="form-control selectpicker" name="country" id="country1">
                                                                    <option>United States</option>
                                                                    <option>Ustralia</option>
                                                                    <option>Eorup</option>
                                                                </select>
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="postcode">Post Code</label>
                                                                <input type="text" name="postcode" id="postcode" className="form-control" placeholder="-" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="btn-groups justify-content-center mt-1" role="group" aria-label="Basic example">
                                                        <a href="/DashboardProfile" type="submit" className="btn btn-primary w150">Checkout</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="col-md-5 col-sm-5 col-xs-12 summary">
                            <h2 className="title">Summary</h2>
                            <ul className="summary-list">
                                <li>
                                    Premium Plan
                                    <span>$5.00</span>
                                </li>
                                <li>
                                    12,000 Impression quota
                                    <span>$0.00</span>
                                </li>
                                <li>
                                    <hr />
                                </li>
                                <li>
                                    Estimated monthly cost
                                    <span>$5.00</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Register;