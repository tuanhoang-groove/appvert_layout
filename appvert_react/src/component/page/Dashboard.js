import React, { Component } from 'react';

class Dashboard extends Component {
    render() {
        return (
            <div>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                    <li className="breadcrumb-item"><a href="">Home</a></li>
                    <li className="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
                <div className="clearfix" />
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                    <h3 className="h3-title">Dashboard</h3>
                    <div className="table-section">
                        <h4 className="h4-title">Top Active Mobile Apps</h4>
                        <table className="table">
                        <thead className="thead-light">
                            <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Mobile App Name</th>
                            <th scope="col">Mobile App Key</th>
                            <th scope="col">Device Type</th>
                            <th scope="col">Display Request</th>
                            <th scope="col">Dismiss Request</th>
                            <th scope="col">Click Ads Count</th>
                            <th scope="col" className="none-sort">Close Ads Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <th scope="row"><a href="/DashboardMobileAppDetail">1H90878</a></th>
                            <td>Angry Bird</td>
                            <td>KH123</td>
                            <td>iOS</td>
                            <td>2100</td>
                            <td>79</td>
                            <td>1780</td>
                            <td>241</td>
                            </tr>
                            <tr>
                            <th scope="row"><a href="/DashboardMobileAppDetail">1H90878</a></th>
                            <td>Angry Bird</td>
                            <td>KH123</td>
                            <td>iOS</td>
                            <td>2100</td>
                            <td>79</td>
                            <td>1780</td>
                            <td>241</td>
                            </tr>
                            <tr>
                            <th scope="row"><a href="/DashboardMobileAppDetail">1H90878</a></th>
                            <td>Angry Bird</td>
                            <td>KH123</td>
                            <td>iOS</td>
                            <td>2100</td>
                            <td>79</td>
                            <td>1780</td>
                            <td>241</td>
                            </tr>
                            <tr>
                            <th scope="row"><a href="/DashboardMobileAppDetail">1H90878</a></th>
                            <td>Angry Bird</td>
                            <td>KH123</td>
                            <td>iOS</td>
                            <td>2100</td>
                            <td>79</td>
                            <td>1780</td>
                            <td>241</td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                    <div className="table-section">
                        <h4 className="h4-title">Top Active Ad Campaigns</h4>
                        <table className="table">
                        <thead className="thead-light">
                            <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Link URL</th>
                            <th scope="col">Type</th>
                            <th scope="col">Display Request</th>
                            <th scope="col" className="none-sort">Dismiss Request</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <th scope="row"><a href="/DashboardAdCampaignsDetail">1H90878</a></th>
                            <td>Angry Bird</td>
                            <td>KH123</td>
                            <td><a href="">http://build.web.appvert.io/campaign/list</a></td>
                            <td>Image</td>
                            <td>79</td>
                            <td>241</td>
                            </tr>
                            <tr>
                            <th scope="row"><a href="/DashboardAdCampaignsDetail">1H90878</a></th>
                            <td>Angry Bird 1</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</td>
                            <td><a href="">http://build.web.appvert.io/campaign/listweb.appvert.io/campaign/list</a></td>
                            <td>Image</td>
                            <td>79</td>
                            <td>241</td>
                            </tr>
                            <tr>
                            <th scope="row"><a href="/DashboardAdCampaignsDetail">1H90878</a></th>
                            <td>Angry Bird</td>
                            <td>KH123</td>
                            <td><a href="">http://build.web.appvert.io/campaign/list</a></td>
                            <td>Image</td>
                            <td>79</td>
                            <td>241</td>
                            </tr>
                            <tr>
                            <th scope="row"><a href="/DashboardAdCampaignsDetail">1H90878</a></th>
                            <td>Angry Bird 1</td>
                            <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</td>
                            <td><a href="">http://build.web.appvert.io/campaign/listweb.appvert.io/campaign/list</a></td>
                            <td>Image</td>
                            <td>79</td>
                            <td>241</td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
                </div>

        );
    }
}

export default Dashboard;