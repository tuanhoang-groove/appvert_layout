import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class DashboardMobileAppCreate extends Component {
    render() {
        return (
            <div>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="">Home</a></li>
                        <li className="breadcrumb-item"><a href="">Mobile Apps List</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Create App</li>
                    </ol>
                </nav>
                <div className="clearfix" />
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <h3 className="h3-title">Create App</h3>
                        <div className="white_panel">
                            <div className="create-app-info">
                                <div className="item">
                                    <i className="icon-id" />
                                    <ul>
                                        <li>ID</li>
                                        <li>KH9GA90S</li>
                                    </ul>
                                </div>
                                <div className="item fail">
                                    <i className="icon-status" />
                                    <ul>
                                        <li>Status</li>
                                        <li>Fail</li>
                                    </ul>
                                </div>
                                <div className="item">
                                    <i className="icon-key" />
                                    <ul>
                                        <li>Appkey</li>
                                        <li>187HK90Q</li>
                                    </ul>
                                </div>
                            </div>
                            <form action="" className="form-style row mt-3">
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="fullname">Name<span>*</span></label>
                                        <i className="icon-form icon_profile" />
                                        <input type="text" name="fullname" id="fullname" className="form-control" placeholder="e.g Nguyen Thi Minh Hoa"  required />
                                    </div>
                                    <div className="form-group none-icon">
                                        <label htmlFor="devicetype">Device Type<span>*</span></label>
                                        <select className="form-control selectpicker" name="devicetype" id="devicetype">
                                            <option>iOS</option>
                                            <option>Android</option>
                                            <option>Window Phone</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="form-group none-icon">
                                        <label htmlFor="description">Description</label>
                                        <textarea className="form-control text-area-2" name="description" id="description" rows={3} defaultValue={""} />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 btn-groups mt-1 row">
                                    <NavLink to="/DashboardMobileAppList" className="btn btn-secondary" >Cancel</NavLink>
                                    <button className="btn btn-primary" type="submit">Create</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default DashboardMobileAppCreate;