import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './../css/Dashboard.css';
import HeaderLanding from './HeaderLanding';

class Register extends Component {
    render() {
        return (
            <div>
                <HeaderLanding/>
                <div className="container">
                    <div className="row justify-content-between">
                        <div className="col-md-5 col-sm-5 col-xs-12 form-register">
                            <h2 className="title">Let’s Get Started</h2>
                            <form action="" className="form-style" data-toggle="validator">
                                <div className="form-group">
                                    <label htmlFor="fullname">Full Name<span>*</span></label>
                                    <i className="icon-form icon_profile" />
                                    <input type="text" name="fullname" id="fullname" className="form-control" placeholder="e.g Nguyen Thi Minh Hoa" data-error="Please fill on this field." required />
                                    <div className="help-block with-errors" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Email<span>*</span></label>
                                    <i className="icon-form icon_mail" />
                                    <input type="email" name="email" id="email" className="form-control" placeholder="e.g hoanguyen@groovetechnology.com" data-error="Bruh, that email address is invalid" required />
                                    <div className="help-block with-errors" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Password<span>*</span></label>
                                    <i className="icon-form icon_lock" />
                                    <input type="password" name="password" data-minlength={8} id="inputPassword" className="form-control" placeholder="At least 8 characters" data-error="Bruh, At least 8 characters" required />
                                    <div className="help-block with-errors" />
                                </div>
                                <div className="form-group mb-1">
                                    <label htmlFor="confirmpassword">Confirm Password<span>*</span></label>
                                    <i className="icon-form icon_lock" />
                                    <input type="password" name="confirmpassword" id="confirmpassword" className="form-control" placeholder="At least 8 characters" data-match="#inputPassword" data-match-error="Whoops, these don't match" required />
                                    <div className="help-block with-errors" />
                                </div>
                                <div className="capcha form-control">
                                    <div className="form-check">
                                        <label className="form-check-label">
                                            <input type="checkbox" className="form-check-input" name="capcha" id="capcha" defaultValue="checkedValue" />
                                            <span className="cr"><i className="cr-icon icon_check" /></span>
                                            I'm not a robot
                            </label>
                                    </div>
                                    <a href=""><img src="/assets/css/img/landing/recapcha.png" alt="Recapcha" /></a>
                                </div>
                                <div className="btn-groups justify-content-between" role="group" aria-label="Basic example">
                                    <NavLink to="/Landing" type="button" className="btn btn-secondary w100">Back</NavLink>
                                    <NavLink to="/Dashboard" type="submit" className="btn btn-primary w150">Continue</NavLink>
                                </div>
                            </form>
                        </div>
                        <div className="col-md-5 col-sm-5 col-xs-12 summary">
                            <h2 className="title">Summary</h2>
                            <ul className="summary-list">
                                <li>
                                    Premium Plan
                        <span>$5.00</span>
                                </li>
                                <li>
                                    12,000 Impression quota
                        <span>$0.00</span>
                                </li>
                                <li>
                                    <hr />
                                </li>
                                <li>
                                    Estimated monthly cost
                        <span>$5.00</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Register;