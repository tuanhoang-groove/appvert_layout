import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class DashboardSubscriptionPlanList extends Component {
    render() {
        return (
            <div>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="">Home</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Subscription Plan List</li>
                    </ol>
                </nav>
                <div className="clearfix" />
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <h3 className="h3-title clearfix">
                            Subscription Plan List
                            <NavLink className="btn btn-primary btn-icon float-right" to="/DashboardSubscriptionPlanCreate" role="button"><i className="icon-add-white" />Create</NavLink>
                        </h3>
                        <div className="table-section">
                            <table className="table mb-1">
                                <thead className="thead-light">
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Cost</th>
                                        <th scope="col">Quota</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Modified</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row"><NavLink to="/DashboardSubscriptionPlanDetail">1H90878</NavLink></th>
                                        <td>Free Plan</td>
                                        <td>$0.00</td>
                                        <td>1,000</td>
                                        <td>For the beginner who want to trial a package</td>
                                        <td><a href="" className="btn btn-smaller btn-danger">Deactive</a></td>
                                        <td>8/28/2018</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><NavLink to="/DashboardSubscriptionPlanDetail">1H90878</NavLink></th>
                                        <td>Premium Plan</td>
                                        <td>$5.00</td>
                                        <td>12,000</td>
                                        <td>For the users with lots of impression</td>
                                        <td><a href="" className="btn btn-smaller btn-success">Active</a></td>
                                        <td>8/28/2018</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><NavLink to="/DashboardSubscriptionPlanDetail">1H90878</NavLink></th>
                                        <td>Profressional Plan</td>
                                        <td>$10.00</td>
                                        <td>25,000</td>
                                        <td>More impression</td>
                                        <td><a href="" className="btn btn-smaller btn-success">Active</a></td>
                                        <td>8/28/2018</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><NavLink to="/DashboardSubscriptionPlanDetail">1H90878</NavLink></th>
                                        <td>Team Plan</td>
                                        <td>$5.00</td>
                                        <td>11,000</td>
                                        <td>For team</td>
                                        <td><a href="" className="btn btn-smaller btn-danger">Deactive</a></td>
                                        <td>8/28/2018</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <nav aria-label="Page navigation example">
                            <ul className="pagination">
                                <li className="page-item">
                                    <a className="page-link page-prev" href="" aria-label="Previous">
                                        <span className="arrow_triangle-left" />
                                        <span className="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li className="page-item"><a className="page-link" href="">1</a></li>
                                <li className="page-item"><a className="page-link" href="">2</a></li>
                                <li className="page-item"><a className="page-link" href="">3</a></li>
                                <li className="page-item"><a className="page-link active" href="">4</a></li>
                                <li className="page-item"><a className="page-link" href="">5</a></li>
                                <li className="page-item"><a className="page-link" href="">6</a></li>
                                <li className="page-item"><a className="page-link" href="">7</a></li>
                                <li className="page-item">
                                    <a className="page-link page-next" href="" aria-label="Next">
                                        <span className="arrow_triangle-right" />
                                        <span className="sr-only">Next</span>
                                    </a>
                                </li>
                                <li className="page-item page-total">Page 3 of 20</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        );
    }
}

export default DashboardSubscriptionPlanList;