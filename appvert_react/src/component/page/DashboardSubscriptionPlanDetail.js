import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class DashboardSubscriptionPlanDetail extends Component {
    render() {
        return (
            <div>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="">Home</a></li>
                        <li className="breadcrumb-item"><a href="">Subscription Plan</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Subscription Plan Detail</li>
                    </ol>
                </nav>
                <div className="clearfix" />
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <h3 className="h3-title">Angry Bird</h3>
                        <div className="white_panel">
                            <form action="" className="form-style row">
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="form-group none-icon">
                                        <label htmlFor="name">Name<span>*</span></label>
                                        <input type="text" name="name" id="name" className="form-control" placeholder="Enter the subscription name"  required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="cost">Cost</label>
                                        <i className="icon-form icon_currency" />
                                        <input type="text" name="cost" id="cost" className="form-control" placeholder="Enter the cost in dollars"  />
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="select-payment-method user-type plan-type">
                                        <p>Plan Type</p>
                                        <div className="form-check form-check-inline">
                                            <label className="form-check-label">
                                                <input className="form-check-input" type="radio" name="plantype"  defaultValue="checkedValue" defaultChecked />
                                                <span className="cr"><i className="cr-icon icon_check" /></span>
                                                <span>Free</span>
                                            </label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <label className="form-check-label">
                                                <input className="form-check-input" type="radio" name="plantype"  defaultValue="checkedValue" />
                                                <span className="cr"><i className="cr-icon icon_check" /></span>
                                                <span>Paid</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="form-group none-icon">
                                        <label htmlFor="quota">Impression Quota<span>*</span></label>
                                        <input type="text" name="quota" id="quota" className="form-control" placeholder="Enter the impression quota"  required />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12">
                                    <div className="form-group none-icon">
                                        <label htmlFor="description">Description</label>
                                        <textarea className="form-control" name="description" id="description" rows={3} defaultValue={""} />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 select-payment-method">
                                    <p>Active</p>
                                    <div className="form-check form-check-inline ml-2">
                                        <label className="form-check-label">
                                            <input className="form-check-input" type="checkbox" name="active"  defaultValue="checkedValue" defaultChecked />
                                            <span className="cr"><i className="cr-icon icon_check" /></span>
                                        </label>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 btn-groups mt-0 row">
                                    <NavLink className="btn btn-secondary mb-0" to="/DashboardSubscriptionPlanList" role="button">Back To List</NavLink>
                                    <button className="btn btn-primary mb-0" type="submit">Create</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default DashboardSubscriptionPlanDetail;