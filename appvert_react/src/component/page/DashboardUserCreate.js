import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class DashboardUserCreate extends Component {
    render() {
        return (
            <div>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="">Home</a></li>
                        <li className="breadcrumb-item"><a href="">User</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
                <div className="clearfix" />
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <h3 className="h3-title">Create User</h3>
                        <div className="white_panel">
                            <form action="" className="form-style row">
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="fullname">Full Name<span>*</span></label>
                                        <i className="icon-form icon_profile" />
                                        <input type="text" name="fullname" id="fullname" className="form-control" placeholder="Enter full name"  required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="phone">Phone</label>
                                        <i className="icon-form icon_phone" />
                                        <input type="text" name="phone" id="phone" className="form-control" placeholder="Enter phone number"  />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="password">Password<span>*</span></label>
                                        <i className="icon-form icon_lock" />
                                        <input type="password" name="password" id="password" className="form-control" placeholder="At least 8 characters"  required />
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="email">Email<span>*</span></label>
                                        <i className="icon-form icon_mail" />
                                        <input type="email" name="email" id="email" className="form-control" placeholder="e.g hoanguyen@groovetechnology.com"  required />
                                    </div>
                                    <div className="form-group none-icon">
                                        <label htmlFor="timezone">Timezone<span>*</span></label>
                                        <select className="form-control selectpicker" name="timezone" id="timezone">
                                            <option>United States</option>
                                            <option>Ustralia</option>
                                            <option>Eorup</option>
                                        </select>
                                    </div>
                                    <div className="form-group mb-1">
                                        <label htmlFor="confirmpassword">Confirm Password<span>*</span></label>
                                        <i className="icon-form icon_lock" />
                                        <input type="password" name="confirmpassword" id="confirmpassword" className="form-control" placeholder="At least 8 characters"  required />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 select-payment-method user-type">
                                    <p>User Type</p>
                                    <div className="form-check form-check-inline">
                                        <label className="form-check-label">
                                            <input className="form-check-input" type="radio" name="usertype" id="" defaultValue="checkedValue" defaultChecked />
                                            <span className="cr"><i className="cr-icon icon_check" /></span>
                                            <span>Administrator</span>
                                        </label>
                                    </div>
                                    <div className="form-check form-check-inline">
                                        <label className="form-check-label">
                                            <input className="form-check-input" type="radio" name="usertype" id="" defaultValue="checkedValue" />
                                            <span className="cr"><i className="cr-icon icon_check" /></span>
                                            <span>User</span>
                                        </label>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 btn-groups mt-0 row">
                                    <NavLink to="/DashboardUserList" className="btn btn-secondary mb-0" role="button">Back To List</NavLink>
                                    <button className="btn btn-primary mb-0" type="submit">Create</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default DashboardUserCreate;