import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class DashboardProfile extends Component {
    render() {
        return (
            <div>
                <div>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item"><a href="">Home</a></li>
                            <li className="breadcrumb-item active" aria-current="page">View Profile</li>
                        </ol>
                    </nav>
                    <div className="clearfix" />
                    <div className="row">
                        <div className="col-md-12 col-sm-12 col-xs-12">
                            <h3 className="h3-title">View Profile</h3>
                            <div className="x_panel">
                                <div className="x_content">
                                    <div className=""role="tabpanel" data-example-id="togglable-tabs">
                                        <ul id="myTab" className="nav nav-tabs bar_tabs" role="tablist">
                                            <li role="presentation" className="active">
                                                <a href="#tab_profile" id="profile-tab" role="tab" data-toggle="tab" aria-expanded="true">Profile</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#tab_subscriptions" role="tab" id="subscriptions-tab" data-toggle="tab" aria-expanded="false">Subscriptions</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#tab_payment-info" role="tab" id="payment-info-tab" data-toggle="tab" aria-expanded="false">Payment Infomation</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#tab_payment-history" role="tab" id="payment-history-tab" data-toggle="tab" aria-expanded="false">Payment History</a>
                                            </li>
                                        </ul>
                                        <div id="myTabContent" className="tab-content">
                                            <div role="tabpanel" className="tab-pane fade active in" id="tab_profile" aria-labelledby="profile-tab">
                                                <div className="row tab_profile">
                                                    <div className="col-md-4 col-sm-4 col-xs-12 grey-box profile-setting">
                                                        <img src="/assets/css/img/profile/avatar.png" alt=""/>
                                                        <span className="name">Sammy Le <span className="email">sammy.le@gmail.com</span></span>
                                                        <NavLink to="/DashboardProfileEdit"className="btn-edit-profile">Edit profile</NavLink>
                                                    </div>
                                                    <div className="col-md-4 col-sm-4 col-xs-12 white-box profile-phone">
                                                        <i className="icon-phone" />
                                                        <span>Phone number <span>(+84) 961 946 765</span></span>
                                                    </div>
                                                    <div className="col-md-4 col-sm-4 col-xs-12 white-box profile-phone">
                                                        <i className="icon-timezone" />
                                                        <span>Timezone <span>(UTC-09:00) Coordinated Universal Time-09</span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" className="tab-pane fade" id="tab_subscriptions" aria-labelledby="subscriptions-tab">
                                                <div className="row tab_subscriptions">
                                                    <div className="col-md-12 col-sm-12 col-xs-12 white-box pd-0">
                                                        <div className="row">
                                                            <div className="col-md-3 col-sm-12 col-xs-12 sub-ttl">
                                                                <h3>Professional</h3>
                                                                <div><p className="price"><span>$</span>19.99</p><p className="month">per month</p></div>
                                                            </div>
                                                            <div className="col-md-3 col-sm-12 col-xs-12 sub-info">
                                                                <h4><span>50,000</span> impressions</h4>
                                                                <ul>
                                                                    <li>Renews on <span>Oct 5, 2018</span></li>
                                                                    <li>Subscribed since <span>Oct 5, 2018</span></li>
                                                                </ul>
                                                            </div>
                                                            <div className="col-md-3 col-sm-12 col-xs-12 sub-payment">
                                                                <h4>Primary Payment Method</h4>
                                                                <ul>
                                                                    <li><i className="icon-visa" /> <span className="card-name">Master Card</span></li>
                                                                    <li>Card Number <span>**** **** **** 4444</span></li>
                                                                </ul>
                                                            </div>
                                                            <div className="col-md-3 col-sm-12 col-xs-12 align-item-center pr-3 text-right">
                                                                <a href="" name="" id="btn-change-payment-method" className="btn btn-small btn-primary" role="button" data-toggle="modal" data-target="#changePaymentMethodModal">Change Payment Method</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12 col-sm-12 col-xs-12 white-box pd-0 mt-3 ">
                                                        <div className="row in-active">
                                                            <div className="col-md-3 col-sm-12 col-xs-12 sub-ttl">
                                                                <h3>Professional</h3>
                                                                <div><p className="price"><span>$</span>19.99</p><p className="month">per month</p></div>
                                                            </div>
                                                            <div className="col-md-3 col-sm-12 col-xs-12 sub-info">
                                                                <h4><span>50,000</span> impressions</h4>
                                                                <ul>
                                                                    <li>Renews on <span>Oct 5, 2018</span></li>
                                                                    <li>Subscribed since <span>Oct 5, 2018</span></li>
                                                                </ul>
                                                            </div>
                                                            <div className="col-md-3 col-sm-12 col-xs-12 sub-payment">
                                                                <h4>Primary Payment Method</h4>
                                                                <ul>
                                                                    <li><i className="icon-visa" /> <span className="card-name">Master Card</span></li>
                                                                    <li>Card Number <span>**** **** **** 4444</span></li>
                                                                </ul>
                                                            </div>
                                                            <div className="col-md-3 col-sm-12 col-xs-12 mt-6 pr-3">
                                                                <a name="" id="btn-change-payment-method" className="btn btn-small btn-primary" href="" role="button">Change Payment Method</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12 col-sm-12 col-xs-12 mt-3 btn-groups">
                                                        <a className="btn btn-danger" href="" role="button">Cancel Subscription</a>
                                                        <a className="btn btn-primary w180" href="" role="button">Change Plan</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" className="tab-pane fade tab_payment-info" id="tab_payment-info" aria-labelledby="payment-info-tab">
                                                <ul className="payment-list">
                                                    <li>
                                                        <div className="card master">
                                                            <h3>Master card</h3>
                                                            <p className="card-number">**** **** **** 2345</p>
                                                            <p className="expires">Expires <span>04/2023</span></p>
                                                            <p className="card-holder-name">Card holder name</p>
                                                            <p className="ribbon ribbon-2">Default</p>
                                                        </div>
                                                        <div className="mt-1 btn-action">
                                                            <a className="btn btn-danger" href="" role="button" data-toggle="modal" data-target="#removeCardConfirm"><i className="icon_trash" /></a>
                                                            <a className="btn btn-primary" href="" role="button"><i className="icon-edit" /></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="card visa">
                                                            <h3>Visa debit</h3>
                                                            <p className="card-number">**** **** **** 6789</p>
                                                            <p className="expires">Expires <span>05/2023</span></p>
                                                            <p className="card-holder-name">Card holder name</p>
                                                        </div>
                                                        <div className="mt-1 btn-action">
                                                            <a className="btn btn-danger" aria-disabled="true" href="" role="button" data-toggle="modal" data-target="#removeCardConfirm"><i className="icon_trash" /></a>
                                                            <a className="btn btn-primary" aria-disabled="true" href="" role="button"><i className="icon-edit" /></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="card paypal">
                                                        </div>
                                                        <div className="mt-1 btn-action">
                                                            <a className="btn btn-danger" aria-disabled="true" href="" role="button" data-toggle="modal" data-target="#removeCardConfirm"><i className="icon_trash" /></a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <a href="/DashboardAddPayment"className="card add-card">
                                                            <span className="btn-add"><i className="icon-add" /></span> <span className="ttl">Add payment method</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div role="tabpanel" className="tab-pane fade" id="tab_payment-history" aria-labelledby="payment-history-tab">
                                                <ul className="payment-history-list">
                                                    <li>
                                                        <div className="row">
                                                            <div className="col-md-5 col-sm-5 col-xs-12">
                                                                <h3>Premium <span>Oct 2018</span></h3>
                                                            </div>
                                                            <div className="col-md-5 col-sm-5 col-xs-12">
                                                                <p><span>50.000</span> impressions</p>
                                                                <p><span>$19.99</span>/month</p>
                                                                <p className="status complete">Completed</p>
                                                            </div>
                                                            <div className="col-md-2 col-sm-2 col-xs-12 text-right">
                                                                <a className="btn btn-primary btn-small" href="" role="button">Get Invoice</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="row">
                                                            <div className="col-md-5 col-sm-5 col-xs-12">
                                                                <h3>Premium <span>Oct 2018</span></h3>
                                                            </div>
                                                            <div className="col-md-5 col-sm-5 col-xs-12">
                                                                <p><span>50.000</span> impressions</p>
                                                                <p><span>$19.99</span>/month</p>
                                                                <p className="status failed">Failed</p>
                                                            </div>
                                                            <div className="col-md-2 col-sm-2 col-xs-12 text-right">
                                                                <a className="btn btn-primary btn-small" href="" role="button">Get Invoice</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="row">
                                                            <div className="col-md-5 col-sm-5 col-xs-12">
                                                                <h3>Premium <span>Oct 2018</span></h3>
                                                            </div>
                                                            <div className="col-md-5 col-sm-5 col-xs-12">
                                                                <p><span>50.000</span> impressions</p>
                                                                <p><span>$19.99</span>/month</p>
                                                                <p className="status failed">Failed</p>
                                                            </div>
                                                            <div className="col-md-2 col-sm-2 col-xs-12 text-right">
                                                                <a className="btn btn-primary btn-small" href="" role="button">Get Invoice</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <nav aria-label="Page navigation example">
                                                    <ul className="pagination">
                                                        <li className="page-item">
                                                            <a className="page-link page-prev" href="" aria-label="Previous">
                                                                <span className="arrow_triangle-left" />
                                                                <span className="sr-only">Previous</span>
                                                            </a>
                                                        </li>
                                                        <li className="page-item"><a className="page-link" href="">1</a></li>
                                                        <li className="page-item"><a className="page-link" href="">2</a></li>
                                                        <li className="page-item"><a className="page-link" href="">3</a></li>
                                                        <li className="page-item"><a className="page-link active" href="">4</a></li>
                                                        <li className="page-item"><a className="page-link" href="">5</a></li>
                                                        <li className="page-item"><a className="page-link" href="">6</a></li>
                                                        <li className="page-item"><a className="page-link" href="">7</a></li>
                                                        <li className="page-item">
                                                            <a className="page-link page-next" href="" aria-label="Next">
                                                                <span className="arrow_triangle-right" />
                                                                <span className="sr-only">Next</span>
                                                            </a>
                                                        </li>
                                                        <li className="page-item page-total">Page 3 of 20</li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default DashboardProfile;