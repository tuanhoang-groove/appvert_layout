import React, { Component } from 'react';

class ModalResetPassword extends Component {
    render() {
        return (
            <div className="modal modal-s fade" id="resetPasswordModal" tabIndex={-1} role="dialog" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Reset Password</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form action="" className="form-style form-style2">
                                <div className="form-group">
                                    <label htmlFor="name">Password<span>*</span></label>
                                    <input type="password" name="password" id="password" className="form-control"  required />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="name">Confirm Password<span>*</span></label>
                                    <input type="password" name="confirmpassword" id="confirmpassword" className="form-control"  required />
                                </div>
                                <div className="mt-2 btn-groups justify-content-center">
                                    <a className="btn btn-danger w150" href="" data-dismiss="modal">Cancel</a>
                                    <button className="btn btn-primary w150" type="submit" data-dismiss="modal">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default ModalResetPassword;