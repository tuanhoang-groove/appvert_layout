import React, { Component } from 'react';

class ModalRemoveCardConfirm extends Component {
    render() {
        return (
            <div className="modal modal-s fade" id="removeCardConfirm" tabIndex={-1} role="dialog" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Confirm Remove</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>Are you sure to remove this payment method?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-danger w150" data-dismiss="modal">Cancel</button>
                            <button type="button" className="btn btn-primary w150">Confirm</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ModalRemoveCardConfirm;