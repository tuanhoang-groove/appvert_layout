import React, { Component } from 'react';

class DashboardMobileAppList extends Component {
    render() {
        return (
            <div>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="">Home</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Mobile Apps List</li>
                    </ol>
                </nav>
                <div className="clearfix" />
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <h3 className="h3-title clearfix">
                            Mobile Apps List
                            <a href="/DashboardMobileAppCreate" className="btn btn-primary btn-icon w150 float-right" ><i className="icon-add-white" />Create App</a>
                        </h3>
                        <div className="table-section">
                            <table className="table mb-1">
                                <thead className="thead-light">
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Mobile App Name</th>
                                        <th scope="col">Mobile App Key</th>
                                        <th scope="col" className="none-sort">Device Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row"><a href="/DashboardMobileAppDetail">1H90878</a></th>
                                        <td>Angry Bird</td>
                                        <td>KH123</td>
                                        <td>iOS</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><a href="/DashboardMobileAppDetail">1H90878</a></th>
                                        <td>Angry Bird</td>
                                        <td>KH123</td>
                                        <td>iOS</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><a href="/DashboardMobileAppDetail">1H90878</a></th>
                                        <td>Angry Bird</td>
                                        <td>KH123</td>
                                        <td>iOS</td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><a href="/DashboardMobileAppDetail">1H90878</a></th>
                                        <td>Angry Bird</td>
                                        <td>KH123</td>
                                        <td>iOS</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <nav aria-label="Page navigation example">
                            <ul className="pagination">
                                <li className="page-item">
                                    <a className="page-link page-prev" href="" aria-label="Previous">
                                        <span className="arrow_triangle-left" />
                                        <span className="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li className="page-item"><a className="page-link" href="">1</a></li>
                                <li className="page-item"><a className="page-link" href="">2</a></li>
                                <li className="page-item"><a className="page-link" href="">3</a></li>
                                <li className="page-item"><a className="page-link active" href="">4</a></li>
                                <li className="page-item"><a className="page-link" href="">5</a></li>
                                <li className="page-item"><a className="page-link" href="">6</a></li>
                                <li className="page-item"><a className="page-link" href="">7</a></li>
                                <li className="page-item">
                                    <a className="page-link page-next" href="" aria-label="Next">
                                        <span className="arrow_triangle-right" />
                                        <span className="sr-only">Next</span>
                                    </a>
                                </li>
                                <li className="page-item page-total">Page 3 of 20</li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>

        );
    }
}

export default DashboardMobileAppList;