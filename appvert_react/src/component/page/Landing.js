import React, { Component } from 'react';
import './Landing.css';
import HeaderLandingWhite from './HeaderLandingWhite';

class Landing extends Component {
    render() {
        return (
            <div>
                <HeaderLandingWhite/>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 col-sm-12 col-xs-12">
                            <h2 className="title-page">Find the Perfect Plan for You <span>Please select your plan</span></h2>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col-md-4 col-sm-4 col-xs-12">
                            <div className="plan-section">
                                <div className="plan-price option-1">
                                    <p className="price"><span>$</span>0</p>
                                    <p className="per-month">Per Month</p>
                                </div>
                                <ul className="plan-accept">
                                    <li><span>10</span> QUOTA</li>
                                    <li className="active"><span>1</span> Lorem ipisum</li>
                                    <li>x</li>
                                    <li>x</li>
                                </ul>
                                <div className="btn-groups justify-content-center mt-2">
                                    <a href="/Register" type="button" className="btn btn-primary w150">Free</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 col-sm-4 col-xs-12">
                            <div className="plan-section">
                                <div className="plan-price option-2">
                                    <p className="price"><span>$</span>19.99</p>
                                    <p className="per-month">Per Month</p>
                                    <p className="ribbon">Popular</p>
                                </div>
                                <ul className="plan-accept">
                                    <li><span>1,000</span> QUOTA</li>
                                    <li className="active"><span>1</span> Lorem ipisum</li>
                                    <li className="active"><span>20</span> Duis autem vel eum</li>
                                    <li>x</li>
                                </ul>
                                <div className="btn-groups justify-content-center mt-2">
                                    <a href="/Register" type="button" className="btn btn-primary w150">Premium</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 col-sm-4 col-xs-12">
                            <div className="plan-section">
                                <div className="plan-price option-3">
                                    <p className="price"><span>$</span>49.99</p>
                                    <p className="per-month">Per Month</p>
                                </div>
                                <ul className="plan-accept">
                                    <li><span>100,000</span> QUOTA</li>
                                    <li className="active"><span>1</span> Lorem ipisum</li>
                                    <li className="active"><span>20</span> Duis autem vel eum</li>
                                    <li className="active"><span>90</span> Duis autem vel eum</li>
                                </ul>
                                <div className="btn-groups justify-content-center mt-2">
                                    <a href="/Register" type="button" className="btn btn-primary w180">Professional</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer>
                    <address>2018 © Appvert</address>
                </footer>
            </div>

        );
    }
}

export default Landing;