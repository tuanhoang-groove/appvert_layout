import React, { Component } from 'react';

class ModalAddTrigger extends Component {
    render() {
        return (
            <div className="modal fade" id="addTriggerModal" tabIndex={-1} role="dialog" aria-labelledby="addTriggerModal" aria-hidden="true">
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Create Trigger</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form action="" className="form-style form-style2">
                                <div className="row">
                                    <div className="col-md-12 col-sm-12 col-xs-12">
                                        <div className="row">
                                            <div className="col-md-12 col-sm-12 col-xs-12">
                                                <div className="form-group">
                                                    <label htmlFor="name">Name<span>*</span></label>
                                                    <input type="text" name="name" id="name" className="form-control" placeholder="" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-6 col-sm-6 col-xs-12">
                                                <div className="form-group icon-right">
                                                    <label htmlFor="name">Valid from<span>*</span></label>
                                                    <i className="icon-form icon_calendar" />
                                                    <input type="text" className="form-control datetimepicker" id="valid-form" />
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="adslotidentifier">Ad Slot Identifier<span>*</span></label>
                                                    <input type="text" name="adslotidentifier" id="adslotidentifier" className="form-control" placeholder="" required />
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="resetfrequency">Reset Frequency<span>*</span></label>
                                                    <select className="form-control selectpicker" name="resetfrequency" id="resetfrequency">
                                                        <option>Reset Ad</option>
                                                        <option>Banner Ad</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-md-6 col-sm-6 col-xs-12">
                                                <div className="form-group icon-right">
                                                    <label htmlFor="name">Valid to<span>*</span></label>
                                                    <i className="icon-form icon_calendar" />
                                                    <input type="text" className="form-control datetimepicker" id="valid-to" />
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="frequency">Frequency<span>*</span></label>
                                                    <select className="form-control selectpicker" name="frequency" id="frequency">
                                                        <option>Reset Ad</option>
                                                        <option>Banner Ad</option>
                                                    </select>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="delaywith">Delay with<span>*</span></label>
                                                    <select className="form-control selectpicker" name="delaywith" id="delaywith">
                                                        <option>Reset Ad</option>
                                                        <option>Banner Ad</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 mt-0 btn-groups">
                                    <a className="btn btn-danger w150" href="" data-dismiss="modal">Cancel</a>
                                    <button className="btn btn-primary w150" type="submit">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default ModalAddTrigger;