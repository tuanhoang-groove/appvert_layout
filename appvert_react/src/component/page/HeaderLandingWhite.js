import React, { Component } from 'react';

class HeaderLanding extends Component {
    render() {
        return (
            <header>
                <h1 className="logo"><img src="/assets/css/img/landing/logo-white-text.png" alt="Appvert" /></h1>
            </header>
        );
    }
}

export default HeaderLanding;