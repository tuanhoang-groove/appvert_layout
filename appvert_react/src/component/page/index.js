import React, { Component } from 'react';

class index extends Component {
    render() {
        return (
            <div className="">
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="">Home</a></li>
                        <li className="breadcrumb-item active" aria-current="page">List Page</li>
                    </ol>
                </nav>
                <div className="clearfix" />
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <h3 className="h3-title">List Page Appvert</h3>
                        <div className="table-section">
                            <table className="table">
                                <thead className="thead-light">
                                    <tr>
                                        <th scope="col">Id</th>
                                        <th scope="col">Page Name</th>
                                        <th scope="col" className="none-sort">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td><a href="/html/landing.html">Landing</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td><a href="/html/register.html">Landing Register</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td><a href="/html/billing.html">Landing Billing</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td><a href="/html/billing_paypal_account.html">Landing Billing Width Paypal Account</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">5</th>
                                        <td><a href="/html/dashboard.html">Dashboard</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">6</th>
                                        <td><a href="/html/dashboard_profile.html">Dashboard Profile</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">7</th>
                                        <td><a href="/html/dashboard_profile_edit.html">Dashboard Profile Edit</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">8</th>
                                        <td><a href="/html/dashboard_mobile_app_list.html">Dashboard Mobile Apps List</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">9</th>
                                        <td><a href="/html/dashboard_mobile_create_app.html">Dashboard Mobile Create App</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">10</th>
                                        <td><a href="/html/dashboard_ad_campaigns_list.html">Dashboard Ad Campaigns List</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">11</th>
                                        <td><a href="/html/dashboard_ad_campaigns_create.html">Dashboard Ad Campaigns Create</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">12</th>
                                        <td><a href="/html/dashboard_ad_campaigns_create_2B.html">Dashboard Ad Campaigns Create 2B</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">13</th>
                                        <td><a href="/html/dashboard_addpayment.html">Dashboard Add Payment</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">14</th>
                                        <td><a href="/html/dashboard_user_list.html">Dashboard User List</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">15</th>
                                        <td><a href="/html/dashboard_user_create_new.html">Dashboard User Create New</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">16</th>
                                        <td><a href="/html/dashboard_profile_user_edit.html">Dashboard Profile User Edit</a></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default index;