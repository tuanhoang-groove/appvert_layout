import React, { Component } from 'react';

class ModalChangePaymentMethod extends Component {
    render() {
        return (
            <div className="modal fade" id="changePaymentMethodModal" tabIndex={-1} role="dialog" aria-hidden="true">
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Change Payment Method</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form action="" className="form-style form-style2">
                                <div role="tabpanel">
                                    <div className="row">
                                        <div className="col-md-12 col-sm-12 col-xs-12 select-payment-method">
                                            <p>Payment Method</p>
                                            <div className="form-check form-check-inline">
                                                <label className="form-check-label" data-target="#paymentPaypal">
                                                    <input className="form-check-input" type="radio" name="paymentmethod" defaultChecked id="paymentPaypal1" defaultValue="checkedValue" />
                                                    <span className="cr"><i className="cr-icon icon_check" /></span>
                                                    <i className="icon-paypal-large" />
                                                </label>
                                            </div>
                                            <div className="form-check form-check-inline">
                                                <label className="form-check-label" data-target="#paymentCredit">
                                                    <input className="form-check-input" type="radio" name="paymentmethod" id="paymentCredit2" defaultValue="checkedValue" />
                                                    <span className="cr"><i className="cr-icon icon_check" /></span>
                                                    <span className="check-label">Credit/ Debit Card</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    {/* Tab panes */}
                                    <div className="tab-content pd-0">
                                        <div role="tabpanel" className="tab-pane active" id="paymentPaypal">
                                            <div className="btn-groups justify-content-center mt-1" role="group" aria-label="Basic example">
                                                <button type="button" className="btn btn-primary w250">Sign in PayPal account</button>
                                            </div>
                                        </div>
                                        <div role="tabpanel" className="tab-pane" id="paymentCredit">
                                            <div className="row">
                                                <div className="col-md-12 col-sm-12 col-xs-12 select-card">
                                                    <div className="form-check form-check-style">
                                                        <label className="form-check-label" data-target="#cardPayment">
                                                            <input type="radio" className="form-check-input" name="selectpayment" id="cardPayment2" defaultValue="checkedValue" defaultChecked />
                                                            <span className="cr"><i className="cr-icon icon_check" /></span>
                                                            Card <span>**** **** *** 6789</span>
                                                        </label>
                                                    </div>
                                                    <div className="form-check form-check-style">
                                                        <label className="form-check-label" data-target="#cardPayment">
                                                            <input type="radio" className="form-check-input" name="selectpayment" id="cardPayment1" defaultValue="checkedValue" />
                                                            <span className="cr"><i className="cr-icon icon_check" /></span>
                                                            Card <span>**** **** *** 6789</span>
                                                        </label>
                                                    </div>
                                                    <div className="form-check form-check-style">
                                                        <label className="form-check-label" data-target="#addNewPayment">
                                                            <input className="form-check-input" type="radio" name="selectpayment" id="addNewPayment2" defaultValue="checkedValue" />
                                                            <span className="cr"><i className="cr-icon icon_check" /></span>
                                                            Add new card
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="tab-content pd-0">
                                                <div role="tabpanel" className="tab-pane active" id="cardPayment">
                                                    <div className="btn-groups justify-content-end mt-1" role="group" aria-label="Basic example">
                                                        <button type="button" className="btn btn-primary w150" data-dismiss="modal">Save</button>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" className="tab-pane" id="addNewPayment">
                                                    <div className="row">
                                                        <div className="col-md-6 col-sm-6 col-xs-12">
                                                            <div className="form-group">
                                                                <label htmlFor="fullname">Cardholder Name<span>*</span></label>
                                                                <input type="text" name="fullname" id="fullname" className="form-control" placeholder="Enter name"  required />
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="expiration">Expiration Date<span>*</span></label>
                                                                <input type="text" name="expiration" id="expiration" className="form-control" placeholder="mm/yy"  required />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6 col-sm-6 col-xs-12">
                                                            <div className="form-group">
                                                                <label htmlFor="cardnumber">Card Number<span>*</span></label>
                                                                <input type="number" name="cardnumber" id="cardnumber" className="form-control" placeholder="--/--/--/--"  required />
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="cvv">CVV Code<span>*</span></label>
                                                                <input type="text" name="cvv" id="cvv" className="form-control" placeholder="-"  required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="address1">Address 1<span>*</span></label>
                                                        <input type="text" name="address1" id="address1" className="form-control" placeholder="At least 8 characters"  required />
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="address2">Address 2<span>*</span></label>
                                                        <input type="text" name="address2" id="address2" className="form-control" placeholder="At least 8 characters"  required />
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-md-6 col-sm-6 col-xs-12">
                                                            <div className="form-group">
                                                                <label htmlFor="country">Country<span>*</span></label>
                                                                <select className="form-control selectpicker" name="country" id="country1">
                                                                    <option>United States</option>
                                                                    <option>Ustralia</option>
                                                                    <option>Eorup</option>
                                                                </select>
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="state">State</label>
                                                                <input type="text" name="state" id="state" className="form-control" placeholder="-" />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6 col-sm-6 col-xs-12">
                                                            <div className="form-group">
                                                                <label htmlFor="country">City<span>*</span></label>
                                                                <select className="form-control selectpicker" name="country" id="country">
                                                                    <option>United States</option>
                                                                    <option>Ustralia</option>
                                                                    <option>Eorup</option>
                                                                </select>
                                                            </div>
                                                            <div className="form-group">
                                                                <label htmlFor="postcode">Post Code</label>
                                                                <input type="text" name="postcode" id="postcode" className="form-control" placeholder="-" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="btn-groups justify-content-end mt-1" role="group" aria-label="Basic example">
                                                        <button type="button" className="btn btn-primary w150" data-dismiss="modal">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default ModalChangePaymentMethod;