import React, { Component } from 'react';

class DashboardAddPayment extends Component {
    render() {
        return (
            <div>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="">Home</a></li>
                        <li className="breadcrumb-item"><a href="">View Profile</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Add Payment</li>
                    </ol>
                </nav>
                <div className="clearfix" />
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <h3 className="h3-title">Add Payment</h3>
                        <div className="white_panel">
                            <h4 className="h4-title">Add your billing information</h4>
                            <form action="" className="form-style form-style2 row mt-3">
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="fullname">Cardholder Name<span>*</span></label>
                                        <input type="text" name="fullname" id="fullname" className="form-control" placeholder="e.g Nguyen Thi Minh Hoa"  required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="expiration">Expiration Date<span>*</span></label>
                                        <input type="text" name="expiration" id="expiration" className="form-control" placeholder="mm/yy"  required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="address1">Address 1<span>*</span></label>
                                        <input type="text" name="address1" id="address1" className="form-control" placeholder="" required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="country">Country<span>*</span></label>
                                        <select className="form-control selectpicker" name="country" id="country">
                                            <option>United States</option>
                                            <option>Ustralia</option>
                                            <option>Eorup</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="state">State<span>*</span></label>
                                        <input type="text" name="state" id="state" className="form-control" placeholder="" />
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="cardnumber">Card Number<span>*</span></label>
                                        <i className="icon-card icon-visa" />
                                        <input type="text" name="cardnumber" id="cardnumber" className="form-control cardnumber" placeholder="--/--/--/--"  required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="cvv">CVV Code<span>*</span></label>
                                        <input type="text" name="cvv" id="cvv" className="form-control" placeholder="-"  required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="address2">Address 2<span>*</span></label>
                                        <input type="text" name="address2" id="address2" className="form-control" placeholder="-"  required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="city">City<span>*</span></label>
                                        <input type="text" name="city" id="city" className="form-control" placeholder="-"  required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="zip">Zip<span>*</span></label>
                                        <input type="text" name="zip" id="zip" className="form-control" placeholder="-"  required />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12 btn-groups mt-1 row">
                                    <a className="btn btn-danger" href="/DashboardProfile" role="button">Cancel</a>
                                    <button className="btn btn-primary" type="submit">Create</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default DashboardAddPayment;