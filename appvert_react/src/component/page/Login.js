import React, { Component } from 'react';

class Login extends Component {
    render() {
        return (
            <div className="justify-content-between">
                <div className="col-lg-3 col-md-4 col-sm-5 col-xs-12 form-login">
                    <h1 className="logo"><img src="/assets/css/img/landing/logo-black-text.png" alt="Appvert" /></h1>
                    <div>
                        <h2 className="title">Sign In To Appvert</h2>
                        <form action="" className="form-style" data-toggle="validator">
                            <div className="notify notify-msg error shake hidden">
                                <div className="notify-in">
                                    <i className="icon_close_alt" />
                                    <p className="message">Error! Password incorrect!</p>
                                    <a href="javascript:void(0)" className="close"><i className="icon_close" /></a>
                                </div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Email<span>*</span></label>
                                <i className="icon-form icon_mail" />
                                <input type="email" name="email" id="email" className="form-control" placeholder="e.g hoanguyen@groovetechnology.com" data-error="Bruh, that email address is invalid" required />
                                <div className="help-block with-errors" />
                            </div>
                            <div className="form-group mb-1">
                                <label htmlFor="password">Password<span>*</span></label>
                                <i className="icon-form icon_lock" />
                                <input type="password" name="password" id="password" data-minlength={8} className="form-control" placeholder="At least 8 characters" data-error="Bruh, At least 8 characters" required />
                                <div className="help-block with-errors" />
                            </div>
                            <div className="form-group form-check remember-check">
                                <label className="form-check-label">
                                    <input type="checkbox" className="form-check-input" name="remember" id="remember" defaultValue="checkedValue" />
                                    <span className="cr"><i className="cr-icon icon_check" /></span>
                                    Remember me
                                </label>
                            </div>
                            <div className="btn-groups justify-content-end" role="group">
                                <a href="/Dashboard" type="submit" className="btn btn-primary w150">Sign In</a>
                            </div>
                        </form>
                    </div>

                    <div className="sign-up">
                        <p>
                            Don't have an account yet ?
                            <a href="/Landing">Sign Up</a>
                        </p>
                    </div>
                </div>
                <div className="col-md-7 col-sm-7 col-xs-12 summary login align-item-end justify-content-center">
                </div>
            </div>

        );
    }
}

export default Login;