import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class ChangePassword extends Component {
    render() {
        return (
            <div>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item"><a href="">Home</a></li>
                        <li className="breadcrumb-item active" aria-current="page">Change Password</li>
                    </ol>
                </nav>
                <div className="clearfix" />
                <div className="row">
                    <div className="col-md-12 col-sm-12 col-xs-12">
                        <h3 className="h3-title">Change Password</h3>
                        <div className="white_panel">
                            <form action="" className="form-style form-style2">
                                <div className="row">
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="name">Password<span>*</span></label>
                                        <input type="password" name="password" id="password" className="form-control" required />
                                    </div>
                                </div>
                                <div className="col-md-6 col-sm-6 col-xs-12">
                                    <div className="form-group">
                                        <label htmlFor="name">Confirm Password<span>*</span></label>
                                        <input type="password" name="confirmpassword" id="confirmpassword" className="form-control" required />
                                    </div>
                                </div>
                                <div className="col-md-12 col-sm-12 col-xs-12">
                                    <div className="mt-1 btn-groups">
                                        <NavLink to="/DashboardProfile" className="btn btn-danger w150" href="">Cancel</NavLink>
                                        <NavLink to="/DashboardProfile" className="btn btn-primary w150" type="submit">Save</NavLink>
                                    </div>
                                </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default ChangePassword;