Appvert Structure:

- appvert_react //Static site with ReactJS
  Command: 
    - Install npm: npm install
    - Run: npm start

- css // CSS data
  - app.css //main css file
  - fonts //fonts data
  - img //image data

- js // JS data
 - app.js // main js file custom

- src //source file sass css

- gulpfile.js
 Command: 
  - gulp sass //Build sass css
  - gulp sass:watch //watch sass css